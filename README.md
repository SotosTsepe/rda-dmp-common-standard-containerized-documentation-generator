# RDA DMP Common Standard - Containerized Documentation Generator

![Build](https://img.shields.io/gitlab/pipeline/SotosTsepe/rda-dmp-common-standard-containerized-documentation-generator/main?style=plastic)

This repository runs the code supplied from the [RDA DMP Common Standard](https://github.com/RDA-DMP-Common/RDA-DMP-Common-Standard/tree/master/rda_dmp_common_standard_doc_generator) repository
in a Go container.  
Its purpose is to provide a running go environment & automate all the installation processes.  


### Prerequisites
1. [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
2. [Docker](https://docs.docker.com/install) and [Docker Compose](https://docs.docker.com/compose/install)

### Instructions
1. Clone the repository:  
`git clone https://gitlab.com/SotosTsepe/rda-dmp-common-standard-containerized-documentation-generator`

2. Change directory into the newly created repo:  
`cd rda-dmp-common-standard-containerized-documentation-generator`

3. Run docker-compose:  
`docker-compose up -d`

4. SSH into the running container:  
`docker exec -it RDA_DMP-Doc_Generator sh`

5. Run the script generator:  
`./rda_dmp_common_standard_doc_generator`

   And you are all set! Check the `output` directory!

##### Note
If you are running on Windows, you may see the container restarting.  
If this is the case:
1. Open git console
2. Run `dos2unix docker-entrypoint.sh`

### Authors
Sotiris Tsepelakis <[stsepelakis@sba-research.org](mailto:stsepelakis@sba-research.org)>
