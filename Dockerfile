FROM tetafro/golang-gcc:latest
RUN apk update && apk add git

RUN go get -u github.com/sirupsen/logrus
RUN go get -u gopkg.in/yaml.v3
RUN go get -u github.com/gocarina/gocsv
RUN go get -u github.com/jinzhu/gorm
RUN go get -u github.com/goki/ki/ki
RUN go get github.com/mattn/go-sqlite3

COPY ./app /app
WORKDIR /app

RUN cd src && go build -o ../rda_dmp_common_standard_doc_generator

COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN ["chmod", "+x", "/docker-entrypoint.sh"]
ENTRYPOINT ["/docker-entrypoint.sh"]
